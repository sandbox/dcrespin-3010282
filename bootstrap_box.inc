<?php

/**
 * @file
 * Webform module layout_box component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_bootstrap_row() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'private' => FALSE,
      'classline' => '',
      'type' => 'row',
    ),
  );
}

function _webform_defaults_bootstrap_col() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'private' => FALSE,
      'classline' => '',
      'lg' => 'col-lg-12',
      'md' => 'col-md-12',
      'sm' => 'col-sm-12',
      'xs' => 'col-xs-12',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_bootstrap_row($component) {
  $form = array();
  $form['display']['classline'] = array(
    '#type' => 'textfield',
    '#title' => t('classes'),
    '#description' => t('Copy paste your classes here (or use dropdown below)'),
    '#default_value' => $component['extra']['classline'],
    '#parents' => array('extra', 'classline'),
  );
  $form['display']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#default_value' => $component['extra']['type'],
    '#parents' => array('extra', 'type'),
    '#options' => array(
      'row' => t('Row'),
    ),
  );
  // Hide the description box.
  $form['extra']['description']['#access'] = FALSE;
  return $form;
}

function _webform_edit_bootstrap_col($component) {
  $form = array();
  $form['display']['classline'] = array(
    '#type' => 'textfield',
    '#title' => t('classes'),
    '#description' => t('Copy paste your classes here (or use dropdown below)'),
    '#default_value' => $component['extra']['classline'],
    '#parents' => array('extra', 'classline'),
  );
  $form['display']['lg'] = array(
    '#type' => 'select',
    '#title' => t('col-lg (if selected COL above)'),
    '#default_value' => $component['extra']['lg'],
    '#parents' => array('extra', 'lg'),
    '#options' => array(
      'null' => t('null'),
      'col-lg-12' => t('col-lg-12'),
      'col-lg-11' => t('col-lg-11'),
      'col-lg-10' => t('col-lg-10'),
      'col-lg-9' => t('col-lg-9'),
      'col-lg-8' => t('col-lg-8'),
      'col-lg-7' => t('col-lg-7'),
      'col-lg-6' => t('col-lg-6'),
      'col-lg-5' => t('col-lg-5'),
      'col-lg-4' => t('col-lg-4'),
      'col-lg-3' => t('col-lg-3'),
      'col-lg-2' => t('col-lg-2'),
      'col-lg-1' => t('col-lg-1'),
    ),
  );
  $form['display']['md'] = array(
    '#type' => 'select',
    '#title' => t('col-md (if selected COL above)'),
    '#default_value' => $component['extra']['md'],
    '#parents' => array('extra', 'md'),
    '#options' => array(
      'null' => t('null'),
      'col-md-12' => t('col-md-12'),
      'col-md-11' => t('col-md-11'),
      'col-md-10' => t('col-md-10'),
      'col-md-9' => t('col-md-9'),
      'col-md-8' => t('col-md-8'),
      'col-md-7' => t('col-md-7'),
      'col-md-6' => t('col-md-6'),
      'col-md-5' => t('col-md-5'),
      'col-md-4' => t('col-md-4'),
      'col-md-3' => t('col-md-3'),
      'col-md-2' => t('col-md-2'),
      'col-md-1' => t('col-md-1'),
    ),
  );
  $form['display']['sm'] = array(
    '#type' => 'select',
    '#title' => t('col-sm (if selected COL above)'),
    '#default_value' => $component['extra']['sm'],
    '#parents' => array('extra', 'sm'),
    '#options' => array(
      'null' => t('null'),
      'col-sm-12' => t('col-sm-12'),
      'col-sm-11' => t('col-sm-11'),
      'col-sm-10' => t('col-sm-10'),
      'col-sm-9' => t('col-sm-9'),
      'col-sm-8' => t('col-sm-8'),
      'col-sm-7' => t('col-sm-7'),
      'col-sm-6' => t('col-sm-6'),
      'col-sm-5' => t('col-sm-5'),
      'col-sm-4' => t('col-sm-4'),
      'col-sm-3' => t('col-sm-3'),
      'col-sm-2' => t('col-sm-2'),
      'col-sm-1' => t('col-sm-1'),
    ),
  );
  $form['display']['xs'] = array(
    '#type' => 'select',
    '#title' => t('col-xs (if selected COL above)'),
    '#default_value' => $component['extra']['xs'],
    '#parents' => array('extra', 'xs'),
    '#options' => array(
      'null' => t('null'),
      'col-xs-12' => t('col-xs-12'),
      'col-xs-11' => t('col-xs-11'),
      'col-xs-10' => t('col-xs-10'),
      'col-xs-9' => t('col-xs-9'),
      'col-xs-8' => t('col-xs-8'),
      'col-xs-7' => t('col-xs-7'),
      'col-xs-6' => t('col-xs-6'),
      'col-xs-5' => t('col-xs-5'),
      'col-xs-4' => t('col-xs-4'),
      'col-xs-3' => t('col-xs-3'),
      'col-xs-2' => t('col-xs-2'),
      'col-xs-1' => t('col-xs-1'),
    ),
  );
  // Hide the description box.
  $form['extra']['description']['#access'] = FALSE;
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_bootstrap_row($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#weight' => $component['weight'],
    '#pre_render' => array('webform_bootstrap_box_prerender'),
    '#webform_component' => $component,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  return $element;
}

function _webform_render_bootstrap_col($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#weight' => $component['weight'],
    '#pre_render' => array('webform_bootstrap_box_prerender'),
    '#webform_component' => $component,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  return $element;
}

/**
 * Pre-render function to set a layout_box ID and classes.
 */
function webform_bootstrap_box_prerender($element) {
  if (isset($element['#webform_component']['extra']['classline']) && $element['#webform_component']['extra']['classline'] != '') {
    $classes = explode(" ", $element['#webform_component']['extra']['classline']);
  }
  if (isset($element['#webform_component']['extra']['type']) && $element['#webform_component']['extra']['type'] == 'row') {
    $classes[] = 'row';
  }
  if (isset($element['#webform_component']['extra']['lg']) && $element['#webform_component']['extra']['lg'] != 'null') {
    $classes[] = $element['#webform_component']['extra']['lg'];
  }
  if (isset($element['#webform_component']['extra']['md']) && $element['#webform_component']['extra']['md'] != 'null') {
    $classes[] = $element['#webform_component']['extra']['md'];
  }
  if (isset($element['#webform_component']['extra']['sm']) && $element['#webform_component']['extra']['sm'] != 'null') {
    $classes[] = $element['#webform_component']['extra']['sm'];
  }
  if (isset($element['#webform_component']['extra']['xs']) && $element['#webform_component']['extra']['xs'] != 'null') {
    $classes[] = $element['#webform_component']['extra']['xs'];
  }
  $classes = array_unique($classes);
  $id = 'webform-component-' . str_replace('_', '-', implode('--', array_slice($element['#parents'], 1)));
  $element['#prefix'] = '<div id="' . $id . '" class="' . implode(' ', $classes) . '">';
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_bootstrap_row($component, $value, $format = 'html') {
  if ($format == 'text') {
    $element = array(
      '#title' => '',
      '#weight' => $component['weight'],
      '#theme_wrappers' => array(),
    );
  }
  else {
    $element = _webform_render_bootstrap_row($component, $value);
  }

  $element['#webform_component'] = $component;
  $element['#format'] = $format;

  return $element;
}

function _webform_display_bootstrap_col($component, $value, $format = 'html') {
  if ($format == 'text') {
    $element = array(
      '#title' => '',
      '#weight' => $component['weight'],
      '#theme_wrappers' => array(),
    );
  }
  else {
    $element = _webform_render_bootstrap_col($component, $value);
  }
  $element['#webform_component'] = $component;
  $element['#format'] = $format;

  return $element;
}